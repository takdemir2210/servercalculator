<?php


namespace Test;

use PHPUnit\Framework\TestCase;
use Taner\lib\ServerCalculator;
use Taner\services\Helper;

/**
 * Class ServerCaculatorTest
 * @package Test
 */
class ServerCaculatorTest extends TestCase
{
    public function testCalculate():void
    {
        try{
            $serverType = ["cpu"=>2,"ram"=>32,"hdd"=>100];
            $virtualMachines = [
                ["cpu"=>1,"ram"=>16,"hdd"=>10],
                ["cpu"=>1,"ram"=>16,"hdd"=>10],
                ["cpu"=>2,"ram"=>32,"hdd"=>100]
            ];

            $serverCalculator = new ServerCalculator();
            $result = $serverCalculator->calculate($serverType,$virtualMachines);
            self::assertGreaterThanOrEqual(0,$result);
        }catch (\Exception $exception){

        }

    }


    public function testCalculateServerTypeException():void
    {
        try{

            $serverType = ["cpuU"=>2,"ramM"=>32,"hddD"=>100];
            $virtualMachines = [
                ["cpu"=>1,"ram"=>16,"hdd"=>10],
                ["cpu"=>1,"ram"=>16,"hdd"=>10],
                ["cpu"=>2,"ram"=>32,"hdd"=>150]
            ];

            $serverCalculator = new ServerCalculator();
            $result = $serverCalculator->calculate($serverType,$virtualMachines);
            self::expectException(\OutOfBoundsException::class);
            self::expectExceptionMessage("Server configuration is wrong!");

        }catch (\Exception $exception){

        }

    }


    public function testCalculateVirtualMachineTypeException():void
    {
        try{

            $serverType = ["cpu"=>2,"ram"=>32,"hdd"=>100];
            $virtualMachines = [
                ["cpuU"=>1,"ramM"=>16,"hddD"=>10],
                ["cpu"=>1,"ram"=>16,"hdd"=>10],
                ["cpu"=>2,"ram"=>32,"hdd"=>100]
            ];

            $serverCalculator = new ServerCalculator();
            $result = $serverCalculator->calculate($serverType,$virtualMachines);
            self::expectException(\OutOfBoundsException::class);
            self::expectExceptionMessage("Virtual machine configuration is wrong!");

        }catch (\Exception $exception){

        }

    }


    public function testCalculateVirtualMachineValueBiggerException():void
    {
        try{

            $serverType = ["cpu"=>2,"ram"=>32,"hdd"=>100];
            $virtualMachines = [
                ["cpuU"=>1,"ramM"=>16,"hddD"=>120],
                ["cpu"=>1,"ram"=>16,"hdd"=>120],
                ["cpu"=>2,"ram"=>32,"hdd"=>150]
            ];

            $serverCalculator = new ServerCalculator();
            $result = $serverCalculator->calculate($serverType,$virtualMachines);
            self::expectException(\Exception::class);
            self::expectExceptionMessage("Virtual machine is too big for that type server.");

        }catch (\Exception $exception){

        }

    }

}