# README #

Server Calculator Service

### What is this repository for? ###

* Calculates server count for the needed virtual machines
* V1.0

### How do I get set up? ###

* composer install
* php -S localhost:8000 index.php

### How to test? ###

* Open a terminal in the project folder
* Run:  ./vendor/bin/phpunit tests

### Who do I talk to? ###

* Taner Akdemir
* taneryzb@hotmail.com
* +90 530 320 06 47