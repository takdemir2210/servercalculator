<?php

namespace Taner\services;

/**
 * Class Logger
 * @package Taner\services
 */
class Logger implements LoggerInterface
{
    /**
     * @param string $logMessage
     * @param string $logType
     */
    function log(string $logMessage, string $logType = "error"): void
    {
        // we symbolize to log to DB
        echo "<pre>";
        print_r([$logMessage, $logType]);
    }
}