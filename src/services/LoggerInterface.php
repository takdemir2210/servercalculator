<?php


namespace Taner\services;


interface LoggerInterface
{
    function log(string $logMessage, string $logType = "error"): void;

}