<?php


namespace Taner\services;

/**
 * Class Helper
 * @package Taner\services
 */
class Helper
{
    /**
     * @param $message
     */
    static function printR($message):void
    {
        echo "<pre>";
        print_r($message);
        exit;
    }

}