<?php


namespace Taner\lib;

use Taner\services\Helper;
use Taner\services\Logger;

/**
 * Class ServerCalculator
 * @package Taner\lib
 */
class ServerCalculator
{

    /**
     * @param $serverType
     * @param $virtualMachines
     * @return int
     * @throws \Exception
     */
    public function calculate($serverType, $virtualMachines): int
    {
            // Checking if servertype configuration is valid or not

            if( !$this->isValidConfig($serverType)){
                throw new \OutOfBoundsException("Server configuration is wrong!");
            }

            $neededTotalCpu = 0;
            $neededTotalRam = 0;
            $neededTotalHdd = 0;

            list("cpu"=>$serverCpu,"ram"=>$serverRam,"hdd"=>$serverHdd) = $serverType;

            foreach ($virtualMachines as $virtualMachine){

                // Checking if virtual machine configuration is valid or not

                if( !$this->isValidConfig($virtualMachine)){
                    throw new \OutOfBoundsException("Virtual machine configuration is wrong!");
                }

                list("cpu"=>$virtualServerCpu,"ram"=>$virtualServerRam,"hdd"=>$virtualServerHdd) = $virtualMachine;

                // Checking if virtual machine configuration is bigger than server configuration
                if($virtualServerCpu>$serverCpu || $virtualServerRam>$serverRam || $virtualServerHdd>$serverHdd){
                    throw new \Exception("Virtual machine is too big for that type server.");
                }

                // if everything is ok, than calculate the needed total resocurce
                $neededTotalCpu+=$virtualServerCpu;
                $neededTotalRam+=$virtualServerRam;
                $neededTotalHdd+=$virtualServerHdd;
            }

            // Rates the sources to the needed and get the max source rate to find the needed server count
            $rates = [round($neededTotalCpu/$serverCpu),round($neededTotalRam/$serverRam),round($neededTotalHdd/$serverHdd)];

            return max($rates);

    }


    /**
     * @param array $type
     * @return bool
     */
    public function isValidConfig(array $type): bool
    {
        return is_array($type) &&
        array_key_exists("cpu",$type) &&
        array_key_exists("ram",$type) &&
        array_key_exists("hdd",$type);
    }
}