<?php
require __DIR__ . '/vendor/autoload.php';
use Taner\lib\ServerCalculator;
use Taner\services\Helper;
use Taner\services\Logger;

$serverCalcualtor = new ServerCalculator();
$serverType = ["cpu"=>2,"ram"=>32,"hdd"=>100];
$virtualMachines = [
    ["cpu"=>1,"ram"=>16,"hdd"=>10],
    ["cpu"=>1,"ram"=>16,"hdd"=>10],
    ["cpu"=>2,"ram"=>32,"hdd"=>100]
];

try{

    $result = $serverCalcualtor->calculate($serverType,$virtualMachines);
    \Taner\services\Helper::printR($result);
}
catch (\OutOfBoundsException $outOfBoundsException){
    $loggerService = new Logger();
    $loggerService->log($outOfBoundsException->getMessage());
}catch (\Exception $exception){Helper::printR($exception->getCode());
    $loggerService = new Logger();
    $loggerService->log($exception->getMessage());
}
